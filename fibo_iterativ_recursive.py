import sys
print('Iterative:')
def fibo_iterative(n):
	a, b = 0, 1
	for i in range(0, n):
		a, b = b, a + b
	return a

result_list_iter =[]
for i in range(100) :
	if fibo_iterative(i) < int(sys.argv[1]):
		print (str(i)+ ": " + str(fibo_iterative(i)))
		result_list_iter.append(fibo_iterative(i))




print('Recursive')
def fibo(n):
	if n<= 1 or n==2 :
		return 1
	else:
		return fibo(n-1)+fibo(n-2)
result_list_recursive = []
for i in range(1,100):
	if fibo(i) < int(sys.argv[1]) :
		print(str(i) + ': ' + str(fibo(i)))
		result_list_recursive.append(fibo(i))
