from fibo_iterative import *
from fibo_recursive import *

def test_fibo():
	assert print_fibo(100) == print_fibo_iterative(100)
