import sys
def fibo_iterative(n):
	a, b = 1 , 1
	for i in range(0, n):
		a, b = b, a + b
	return a

def print_fibo_iterative(n) :
	result_list_iter =[]
	for i in range(n) :
		if not fibo_iterative(i) < int(sys.argv[1]) :
			break
		print (str(i)+ ": " + str(fibo_iterative(i)))
		result_list_iter.append(fibo_iterative(i))

	return result_list_iter

print_fibo_iterative(100)

