def fibo_iterative(n):
	a, b = 0, 1
	for i in range(0, n):
		a, b = b, a + b
	return a

for i in range(100) :
	if fibo_iterative(i) < 100:
		print (str(i)+ ": " + str(fibo_iterative(i)))
